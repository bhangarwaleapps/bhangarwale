package com.app.bhangarwale.exception;

public class InvalidPhoneNumberException extends Exception{

	public InvalidPhoneNumberException(String message) {
		super(message);
	}
}
