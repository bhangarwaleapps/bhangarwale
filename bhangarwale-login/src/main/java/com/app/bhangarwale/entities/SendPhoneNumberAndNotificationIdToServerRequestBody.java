package com.app.bhangarwale.entities;

public class SendPhoneNumberAndNotificationIdToServerRequestBody {

	private String phoneNumber;

	public String getPhoneNumber() {
		return phoneNumber.trim();
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber.trim();
	}
	
	
}
