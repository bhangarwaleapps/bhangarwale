package com.app.bhangarwale.entities;

import com.app.bhangarwale.contract.BhangarwaleResponseBody;
import com.app.bhangarwale.contract.ResponseCodeBody;

public class SendPhoneNumberAndNotificationIdToServerResponseBody extends BhangarwaleResponseBody{

	public SendPhoneNumberAndNotificationIdToServerResponseBody(ResponseCodeBody responseCodeBody) {
		super(responseCodeBody, null);
	}

	@Override
	public ResponseCodeBody getResponseCodeBody() {
		return responseCodeBody;
	}

	@Override
	public Object getResponse() {
		return response;
	}

}
