package com.app.bhangarwale.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "tbl_user_notification_junction")
public class UserNotificationJunction {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private int id;
	
	@Column(name = "notification_key")
	private int notificationKey;
	
	@Column(name = "user_key")
	private int userKey;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getNotificationKey() {
		return notificationKey;
	}

	public void setNotificationKey(int notificationKey) {
		this.notificationKey = notificationKey;
	}

	public int getUserKey() {
		return userKey;
	}

	public void setUserKey(int userKey) {
		this.userKey = userKey;
	}
	
	
	
	

}
