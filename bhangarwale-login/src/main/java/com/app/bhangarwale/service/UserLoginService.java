package com.app.bhangarwale.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import com.app.bhangarwale.contract.ResponseCodeBody;
import com.app.bhangarwale.dao.UserLoginDao;
import com.app.bhangarwale.entities.SendPhoneNumberAndNotificationIdToServerRequestBody;
import com.app.bhangarwale.entities.SendPhoneNumberAndNotificationIdToServerResponseBody;
import com.app.bhangarwale.exception.InvalidPhoneNumberException;
import com.app.bhangarwale.validators.PhoneNumberValidator;

@Service
public class UserLoginService {

	@Autowired
	UserLoginDao userLoginDao;

	/**
	 * @param SendPhoneNumberAndNotificationIdToServerRequestBody sendPhoneNumberAndNotificationIdToServerRequestBody
	 * @return ResponseEntity<SendPhoneNumberAndNotificationIdToServerResponseBody> responseEntity
	 */
	public ResponseEntity<SendPhoneNumberAndNotificationIdToServerResponseBody> sendPhoneNumberAndNotificationIdToServer(
			SendPhoneNumberAndNotificationIdToServerRequestBody sendPhoneNumberAndNotificationIdToServerRequestBody) {

		try {
			String phoneNumber = sendPhoneNumberAndNotificationIdToServerRequestBody.getPhoneNumber();
			//Here, number is invalid then we throw exception 
			if (PhoneNumberValidator.isInValidNumber(phoneNumber)) {
				throw new InvalidPhoneNumberException("Enter Valid Phone Number");
			}			
			//Here, check number is available in database
			if (userLoginDao.isUserAlreadyAvailable(sendPhoneNumberAndNotificationIdToServerRequestBody)) {
				ResponseCodeBody responseCodeBody = new ResponseCodeBody(ResponseCodeBody.ResponseCode.SUCCESS,"You are already register");
				SendPhoneNumberAndNotificationIdToServerResponseBody sendPhoneNumberAndNotificationIdToServerResponseBody
				= new SendPhoneNumberAndNotificationIdToServerResponseBody(
						responseCodeBody);
				return new ResponseEntity<SendPhoneNumberAndNotificationIdToServerResponseBody>(
						sendPhoneNumberAndNotificationIdToServerResponseBody, HttpStatus.OK);
			} else {
				ResponseCodeBody responseCodeBody = new ResponseCodeBody(ResponseCodeBody.ResponseCode.SUCCESS,"Ohh you are new here");
				SendPhoneNumberAndNotificationIdToServerResponseBody sendPhoneNumberAndNotificationIdToServerResponseBody
				= new SendPhoneNumberAndNotificationIdToServerResponseBody(
						responseCodeBody);
				return new ResponseEntity<SendPhoneNumberAndNotificationIdToServerResponseBody>(
						sendPhoneNumberAndNotificationIdToServerResponseBody, HttpStatus.OK);
			}
		} catch (InvalidPhoneNumberException e) {
			ResponseCodeBody responseCodeBody = new ResponseCodeBody(ResponseCodeBody.ResponseCode.FAIL,e.getLocalizedMessage());
			SendPhoneNumberAndNotificationIdToServerResponseBody sendPhoneNumberAndNotificationIdToServerResponseBody
			= new SendPhoneNumberAndNotificationIdToServerResponseBody(
					responseCodeBody);
			return new ResponseEntity<SendPhoneNumberAndNotificationIdToServerResponseBody>(
					sendPhoneNumberAndNotificationIdToServerResponseBody, HttpStatus.INTERNAL_SERVER_ERROR);
		} catch (Throwable e) {
			ResponseCodeBody responseCodeBody = new ResponseCodeBody(ResponseCodeBody.ResponseCode.FAIL,e.getLocalizedMessage());
			SendPhoneNumberAndNotificationIdToServerResponseBody sendPhoneNumberAndNotificationIdToServerResponseBody
			= new SendPhoneNumberAndNotificationIdToServerResponseBody(
					responseCodeBody);
			return new ResponseEntity<SendPhoneNumberAndNotificationIdToServerResponseBody>(
					sendPhoneNumberAndNotificationIdToServerResponseBody, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
