package com.app.bhangarwale.dao;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.app.bhangarwale.entities.SendPhoneNumberAndNotificationIdToServerRequestBody;
import com.app.bhangarwale.entities.UserDetails;
import com.app.bhangarwale.exception.ToDo;
import com.sun.xml.bind.v2.TODO;

@Repository
@Transactional
public class UserLoginDao {

	@Autowired
	private SessionFactory sessionFactory;

	protected Session getSession() {
		return sessionFactory.getCurrentSession();
	}
	
	public Boolean isUserAlreadyAvailable(SendPhoneNumberAndNotificationIdToServerRequestBody sendPhoneNumberAndNotificationIdToServerRequestBody) {
		boolean res = false;
		try {
			Session session = getSession();
			Criteria criteria = session.createCriteria(UserDetails.class);
			String phoneNumber = sendPhoneNumberAndNotificationIdToServerRequestBody.getPhoneNumber();
			criteria.add(Restrictions.eq("phoneNumber",phoneNumber));
			if (criteria.list().isEmpty()) {
				
				  UserDetails userDetails = new UserDetails();
				  userDetails.setPhoneNumber(phoneNumber);
				  session.save(userDetails);
				  				
			}else {
				res = true;
			}
		} catch (HibernateException e) { 
			e.printStackTrace();
		}
		return res;
	}
	
}
