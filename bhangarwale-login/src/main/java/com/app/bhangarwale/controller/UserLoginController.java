package com.app.bhangarwale.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.app.bhangarwale.contract.BhangarwaleRequestBody;
import com.app.bhangarwale.contract.ResponseCodeBody;
import com.app.bhangarwale.entities.SendPhoneNumberAndNotificationIdToServerRequestBody;
import com.app.bhangarwale.entities.SendPhoneNumberAndNotificationIdToServerResponseBody;
import com.app.bhangarwale.exception.InvalidPhoneNumberException;
import com.app.bhangarwale.service.UserLoginService;

@RestController
@RequestMapping("/login")
public class UserLoginController {

	@Autowired
	private UserLoginService userLoginService;
	
	@RequestMapping(value = "/sendPhoneNumberAndNotificationIdToServer", method = RequestMethod.POST)
	public ResponseEntity<SendPhoneNumberAndNotificationIdToServerResponseBody> sendPhoneNumberAndNotificationIdToServer(
			@RequestBody SendPhoneNumberAndNotificationIdToServerRequestBody sendPhoneNumberAndNotificationIdToServerRequestBody) {

		
		return userLoginService.sendPhoneNumberAndNotificationIdToServer(sendPhoneNumberAndNotificationIdToServerRequestBody);
		
		
		
		/*try {

			if (sendPhoneNumberAndNotificationIdToServerRequestBody.getPhoneNumber().length() != 10) {
				throw new InvalidPhoneNumberException("Enter Valid Phone Number");
			}
			
			ResponseCodeBody responseCodeBody = ResponseCodeBody.SUCCESS;
			responseCodeBody.setMessage("Ohh you are new here");
			User user = new User("1222");

			SendPhoneNumberAndNotificationIdToServerResponseBody sendPhoneNumberAndNotificationIdToServerResponseBody = new SendPhoneNumberAndNotificationIdToServerResponseBody(
					responseCodeBody, user);

			return new ResponseEntity<SendPhoneNumberAndNotificationIdToServerResponseBody>(
					sendPhoneNumberAndNotificationIdToServerResponseBody, HttpStatus.OK);

		}

		catch (InvalidPhoneNumberException e) {

			ResponseCodeBody responseCodeBody = ResponseCodeBody.FAIL;
			responseCodeBody.setMessage(e.getLocalizedMessage());

			SendPhoneNumberAndNotificationIdToServerResponseBody sendPhoneNumberAndNotificationIdToServerResponseBody = new SendPhoneNumberAndNotificationIdToServerResponseBody(
					responseCodeBody, null);

			return new ResponseEntity<SendPhoneNumberAndNotificationIdToServerResponseBody>(
					sendPhoneNumberAndNotificationIdToServerResponseBody, HttpStatus.INTERNAL_SERVER_ERROR);

		}

		catch (Throwable e) {

			ResponseCodeBody responseCodeBody = ResponseCodeBody.FAIL;

			SendPhoneNumberAndNotificationIdToServerResponseBody sendPhoneNumberAndNotificationIdToServerResponseBody = new SendPhoneNumberAndNotificationIdToServerResponseBody(
					responseCodeBody, null);

			return new ResponseEntity<SendPhoneNumberAndNotificationIdToServerResponseBody>(
					sendPhoneNumberAndNotificationIdToServerResponseBody, HttpStatus.INTERNAL_SERVER_ERROR);

		}*/

	}

}
