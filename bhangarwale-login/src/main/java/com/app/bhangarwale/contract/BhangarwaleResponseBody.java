package com.app.bhangarwale.contract;

import java.io.Serializable;

public abstract class BhangarwaleResponseBody<Response> implements Serializable{
	
	protected ResponseCodeBody responseCodeBody;
	protected Response response;

	protected BhangarwaleResponseBody(ResponseCodeBody responseCodeBody,Response response) {
		this.responseCodeBody = responseCodeBody;
		this.response = response;
	}

	public abstract ResponseCodeBody getResponseCodeBody();

	public abstract Response getResponse();
	
}
