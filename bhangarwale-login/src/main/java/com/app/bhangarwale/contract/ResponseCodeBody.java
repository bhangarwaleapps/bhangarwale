package com.app.bhangarwale.contract;

public class ResponseCodeBody {

	static public enum ResponseCode{
		SUCCESS,FAIL
	}
	
	private String message;
	private ResponseCode responseCode;

	public ResponseCodeBody(ResponseCode responseCode,String message) {
		this.responseCode = responseCode;
		this.message = message;
	}
	
	public void setMessage(String message) {
		this.message = message;
	}

	public String getMessage() {
		return this.message;
	}

	public ResponseCode getResponseCode() {
		return responseCode;
	}

	public void setResponseCode(ResponseCode responseCode) {
		this.responseCode = responseCode;
	}
		
}
